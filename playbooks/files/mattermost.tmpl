{{ define "__alert_severity" -}}
    {{- if eq .CommonLabels.severity "critical" -}}
    **Severity:** `Critical` :scream:
    {{- else if eq .CommonLabels.severity "warning" -}}
    **Severity:** `Warning` :warning:
    {{- else if eq .CommonLabels.severity "info" -}}
    **Severity:** `Info`
    {{- else -}}
    **Severity:** :shrug: {{ .CommonLabels.severity }}
    {{- end }}
{{- end }}

{{ define "mattermost.color" -}}
    {{ if eq .Status "firing" -}}
        {{ if eq .CommonLabels.severity "warning" -}}
            warning
        {{- else if eq .CommonLabels.severity "critical" -}}
            danger
        {{- else -}}
            #439FE0
        {{- end -}}
    {{ else -}}
    good
    {{- end }}
{{- end }}

{{ define "mattermost.title" -}}
  [{{ .Status | toUpper -}}
  {{ if eq .Status "firing" }}:{{ .Alerts.Firing | len }}{{- end -}}
  ] {{ .CommonLabels.alertname }}
{{- end }}

{{ define "mattermost.text" -}}
  {{ template "__alert_severity" . }}
  {{- if (index .Alerts 0).Annotations.message }}
  {{- "\n" -}}
  *Message:* {{ (index .Alerts 0).Annotations.message }}
  {{- end }}
  {{ range .Alerts }}
    {{- "\n" -}}
    {{- if .Annotations.summary }}
    {{- "\n" -}}
    **Summary:** {{ .Annotations.summary }}
    {{- end }}
    {{- if .Annotations.description }}
    {{- "\n" -}}
    Description: {{ .Annotations.description }}
    {{- end }}
    {{- if .Annotations.dashboard }}
    {{- "\n" -}}
    [Look at metrics]({{ .Annotations.dashboard }})
    {{- end }}
    {{- "\n" -}}
    {{- "***" -}}
  {{- end }}
  {{- "\n" -}}
{{- end }}
