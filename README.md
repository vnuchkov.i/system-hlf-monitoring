# application-hlf-two-org

TODO

## TOC

- [system-hlf-two-org](#system-hlf-two-org)
  - [TOC](#toc)
  - [Description](#description)
  - [HLA](#hla)
  - [Congfiguration](#congfiguration)
    - [Groups](#groups)
    - [Variables](#variables)
  - [YubiHSM](#yubihsm)
  - [License](#license)
  - [Links](#links)

## Description

Данный код реализует полнофункциональную сеть hlf для двух организаций.  
Код предусматривает добавление дополнительных организаци.  

## HLA

![hla](hla.drawio.svg)

## Congfiguration

### Groups

```
    - org0_bastion
    - org0_ca
    - org0_tool
    - org0_peer_public
    - org0_peer_private
    - org0_orderer_public
    - org0_orderer_private
    - org0_share
```

```
    - org1_bastion
    - org1_ca
    - org1_tool
    - org1_peer_public
    - org1_peer_private
    - org1_orderer_public
    - org1_orderer_private
```

### Variables
 
* sys_user_ansible_ssh_pubs
* bundle_disk_dev
* vault_docker_auths

## YubiHSM

Для запуска локального коннектора с YubiHSM необходимо выполнить следующее:
```bash
export deploy=stage
ansible-playbook -i inventory/$deploy yubihsm.yml -e env=$deploy
```

## License

Apache 2.0

## Links

* No
